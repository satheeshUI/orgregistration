import PouchDB from 'pouchdb';

const db = new PouchDB('organisation');

export function CreateOrganisation(payload) {
    return db.bulkDocs(payload).then(res => {
        return Promise.resolve(res)
    })
}

export function getOrganisation(payload) {
    return db.get(payload).then(res => {
        return Promise.resolve(res)
    })
}