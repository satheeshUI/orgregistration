import React, { Fragment }  from 'react';
import { Form,
         Input,
         InputNumber,
         RadioGroup,
         Radio,
         SelectPicker,
         CheckboxGroup,
         Checkbox } from 'rsuite';
import ReactFlagsSelect from 'react-flags-select';
import Parser from 'html-react-parser';
import ImageUploader from 'react-images-upload';
import PhoneInput from 'react-phone-number-input'
import OtpInput from './../lib';


function FormComponent(props) {
    const { formFields, setValueHandler, formDetails, numInputs, handleOtpChange, onDrop } = props
    return(
        <Form fluid>
            { formFields.map((field, fieldIndex) => {
                return <div className="input-wrapper" key={fieldIndex}>
                    { field.type !== 'checkbox' && field.type !== 'imageupload' ? 
                    <label>{ field.label } 
                        { field.validations && field.validations.length ? <span className="error" style={{paddingLeft: "2px", fontSize: "14px"}}>*</span> : null }
                    </label> : null
                     } 
                    { field.type === 'text' && !field.isPhone ?
                        ( 
                            <Fragment>
                               <Input value={formDetails[field.model] ? formDetails[field.model] : ''} onChange={(e) => setValueHandler(field.model, e)} />
                            </Fragment>
                        ) : field.type === 'text' && field.isPhone ? (
                            <Fragment>
                               <PhoneInput
                                value={formDetails[field.model] ? formDetails[field.model] : ''}
                                international
                                defaultCountry={formDetails['country'] ? formDetails['country'] : "IN"}
                                onChange={(e) => setValueHandler(field.model, e)} />
                            </Fragment>
                        ) : field.type === 'radio' ?
                        (
                            <RadioGroup name="radioList" value={formDetails[field.model] ? formDetails[field.model] : ''} onChange={(e) => setValueHandler(field.model, e)}>
                                { field.allowedValues.map((radio, radioIndex) => {
                                    return <Radio value={radio} key={radioIndex}>{radio}</Radio>
                                }) }
                            </RadioGroup>
                        ) : field.type === 'selectbox' && !field.isCountry ?
                        (
                            <SelectPicker data={field.allowedValues} value={formDetails[field.model] ? formDetails[field.model] : ''} style={{ width: '100%' }} onChange={(e) => setValueHandler(field.model, e)} />
                        ) : field.type === 'selectbox' && field.isCountry ?
                            <ReactFlagsSelect defaultCountry="IN" searchable  onSelect={(e) => setValueHandler(field.model, e)} /> : 
                        field.type === 'otp' ? 
                        (
                            <div className="otp-wrapper">
                                <OtpInput
                                className="test"
                                inputStyle={{
                                        width: '65px',
                                        height: '65px',
                                        fontSize: '2em',
                                        borderRadius: 4,
                                        border: '1px solid rgba(0,0,0,0.3)',
                                    }}
                                    numInputs={numInputs}
                                    errorStyle="error"
                                    onChange={handleOtpChange}
                                    shouldAutoFocus
                                    />
                            </div>
                        ) : field.type === 'checkbox' ? 
                        (
                        <CheckboxGroup value={formDetails[field.model]} onChange={(value) => setValueHandler(field.model, value)}>
                            <Checkbox value={true}> { Parser(field.label) }</Checkbox>
                        </CheckboxGroup>
                        ) : field.type === 'imageupload' ? 
                        (
                            <ImageUploader
                                withIcon={field.withIcon}
                                buttonText={field.label}
                                onChange={onDrop}
                                imgExtension={field.extensions}
                                maxFileSize={field.maxFileSize}
                                singleImage={field.isSingleImage}
                                withPreview={field.withPreview}
                                withLabel={field.withLabel}
                            />
                        ) : field.type === 'number' ? 
                        (
                            <InputNumber value={formDetails[field.model] ? formDetails[field.model] : ''} onChange={(e) => setValueHandler(field.model, e)} />
                        ) : null
                    }
                    <span className="error">{field.errorMessage}</span>
                </div>
            }) }
        </Form>
    )
}

export default FormComponent