import React from 'react';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import 'rsuite/dist/styles/rsuite-default.css';
import 'react-flags-select/scss/react-flags-select.scss';
import 'react-phone-number-input/style.css';
import './assets/scss/main.scss';

// modules
import Registration from './modules/Registration';
import Success from './modules/Success';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" component={Registration} exact />
          <Route path="/success/:id" component={Success} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
