import React, { useEffect, useState } from 'react';
import { getOrganisation } from './../backend';
import Lottie from 'react-lottie';
import { Link } from 'react-router-dom';
import SuccessAnim from './../mock/success.json';

const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: SuccessAnim,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

export default function Success(props) {
    const [organisation, setOrganisation] = useState(0)
    useEffect(() => {
        getOrganisation(props.match.params.id).then(res => {
            setOrganisation(res)
        })
    }, [])
    return(
        <div className="success-wrapper">
            <Lottie options={defaultOptions}
              height={400}
              width={400}
              isStopped={false}
              isPaused={false}/>
              <div className="success-wrapper__details">
                <h3 className="success-wrapper__details__title">Hi {organisation.fullName}!</h3>
                <p>Thanks for registering your company. Your Company has been registered successfully.</p>
                <Link to="/" className="success-wrapper__details__link">Back to Home</Link>
              </div>
        </div>
    )
}