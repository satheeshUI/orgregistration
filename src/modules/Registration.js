import React, { Fragment } from 'react';
import { Steps,
         Panel,
         ButtonGroup,
         Button,
         Notification
        } from 'rsuite';
import formData from './../mock/formData.json';
import _ from 'lodash';
import { CreateOrganisation } from './../backend';
import { Link } from 'react-router-dom';

// components
import FormComponent from './../components/FormComponent';

function open(otp) {
    Notification.open({
      title: 'OTP Message',
      description: "Your otp for registering the organisation is " + otp
    });
  }

class RegistrationComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            stepTitles: [
                { title: 'Personal Details', buttonText: "Next" },
                { title: 'Company Details', buttonText: "Send OTP" },
                { title: 'Email Verification', buttonText: "Verify" },
            ],
            activeStep: 0,
            formDetails: {},
            inVokeError: false,
            otp: '',
            numInputs: 5,
            otpVal: "",
            pictures: []
        }
    }

    // update otp
    handleOtpChange = otp => {
        this.setState({ otp });
    };

    // submit record
    handleSubmit = () => {
        if(this.state.otp == this.state.otpVal) {
            CreateOrganisation([this.state.formDetails]).then(res => {
                this.props.history.push(`/success/${res[0].id}`)
            })
        }
    };

    // image upload
    onDrop = (picture) => {
        this.setState({
            pictures: this.state.pictures.concat(picture),
        });
    }

    // form validation
    isValideForm() {
        const { formDetails, activeStep, inVokeError } = this.state
        _.forEach(formData[activeStep].formFields, el => {
            let isRequired = _.includes(el.validations, 'required')
            let isEmail = _.includes(el.validations, 'email')
            if(isRequired) {
                if(!formDetails[el.model]) {
                    el.errorMessage = el.label + " is required."
                    this.setState({ inVokeError: !inVokeError })
                } else {
                    el.errorMessage = ""
                    this.setState({ inVokeError: !inVokeError })
                }
            }
            if(isEmail) {
                if (formDetails[el.model] && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(formDetails[el.model])) {
                    el.errorMessage = ""
                    this.setState({ inVokeError: !inVokeError })
                } else if(!formDetails[el.model]) {
                    el.errorMessage = el.label + " is required."
                    this.setState({ inVokeError: !inVokeError })
                } else {
                    el.errorMessage = el.label + " is not valid."
                    this.setState({ inVokeError: !inVokeError })
                }
            }
        })
        let errorCount = _.filter(formData[activeStep].formFields, el => {
            return el.errorMessage && el.errorMessage !== ''
        }).length
        return errorCount === 0 ? true : false        
    }

    // next step
    onNext = () => {
        if(this.isValideForm()) {
            if(this.state.activeStep < formData.length - 1) {
                this.setState({ activeStep: this.state.activeStep + 1 })
            }
                if(this.state.activeStep === 1) {
                    this.setState({ otpVal: Math.floor(Math.random()*90000) + 10000 }, () => open(this.state.otpVal))
                    
                } else if(this.state.activeStep === 2) {
                    this.handleSubmit()
                }
        }
    }    
    
    // previous step
    onPrevious = () => {
        this.setState({ activeStep: this.state.activeStep - 1 })
    }

    // set form field value handler
    setValueHandler = (name, item) => {
        const { formDetails } = this.state
        formDetails[name] = item
        this.setState({ formDetails }, () => this.isValideForm())
    }

    render() {
        const { stepTitles, activeStep, formDetails, otp, numInputs } = this.state
        return(
            <Fragment>
                <header>
                    <div className="steps-wrapper">
                        <Steps current={activeStep} className="steps-wrapper__steps">
                            { stepTitles.map((step, stepIndex) => {
                                return <Steps.Item title={step.title} key={stepIndex} className="steps-wrapper__steps__item" />
                            }) }
                        </Steps>
                    </div>
                </header>
                <div className="registration-form">
                    <Panel header={`Step: ${activeStep + 1}`}>
                        <div className="registration-form__header">
                            <h2 className="registration-form__header__title">{ formData[activeStep].title }</h2>
                            <p>{ formData[activeStep].desc } { activeStep === 2 ? ( <span><b>{ formDetails['emailId'] }</b>. Please enter it below</span> ) : null }</p>
                        </div>

                        <div className="registration-form__body">
                            <FormComponent
                                formFields={formData[activeStep].formFields}
                                formDetails={formDetails}
                                otp={otp}
                                numInputs={numInputs}
                                setValueHandler={this.setValueHandler}
                                handleOtpChange={this.handleOtpChange}
                                handleSubmit={this.handleSubmit}
                                onDrop={this.onDrop} />
                                <ButtonGroup className="registration-form__body__btnsgroup">
                                    { activeStep !== 0 ? 
                                    <Button onClick={this.onPrevious} style={{width: "100px", marginRight: "15px"}}>
                                        Back
                                    </Button>:null
                                    }                        
                                    <Button onClick={this.onNext} className="form-btn" disabled={activeStep === 1 && ((formDetails.terms && !formDetails.terms.length) || !formDetails.terms)}>
                                        { stepTitles[activeStep].buttonText }
                                    </Button>
                                </ButtonGroup>
                                {
                                    activeStep === 2 ? (
                                        <div className="otp-footer">
                                            Didn't receive the email? Check your spam filter for an email from <Link to="/">{formDetails['emailId']}</Link>
                                        </div>
                                    ) : null
                                }
                        </div>
                        {
                            activeStep === 0 ? (
                                <div className="registration-form__footer">
                                    Already have an account <Link to="/">Log in</Link>
                                </div>
                            ) : null
                        }
                    </Panel>
                </div>
            </Fragment>
        )
    }
}

export default RegistrationComponent